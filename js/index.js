const navToggle = document.querySelector('.nav-toggle');
const navLinks = document.querySelectorAll('.nav__link')

navToggle.addEventListener('click', () => {
    document.body.classList.toggle('nav-open');
});

navLinks.forEach(link => {
    link.addEventListener('click', () => {
        document.body.classList.remove('nav-open');
    })
})

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}

$(document).ready(function() {

    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var sync3 = $("#sync_alpenpark");
    var sync4 = $("#sync_rekamore");
    var sync5 = $("#sync_redbar");
    var sync6 = $("#sync_interior");
    var sync7 = $("#sync_jazzcafe");

    sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });

        $(".next").click(function(){
        sync1.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync1.trigger('owl.prev');
    })

    sync2.owlCarousel({
        items : 6,
        itemsDesktop      : [1199,6],
        itemsDesktopSmall     : [979,6],
        itemsTablet       : [768,4],
        itemsMobile       : [479,3],
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
        el.find(".owl-item").eq(0).addClass("synced");
        }
    });

    sync3.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });

    $(".next").click(function(){
        sync3.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync3.trigger('owl.prev');
    })

    sync4.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
    $(".next").click(function(){
        sync4.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync4.trigger('owl.prev');
    })

    sync5.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
    $(".next").click(function(){
        sync5.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync5.trigger('owl.prev');
    })

    sync6.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
    $(".next").click(function(){
        sync6.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync6.trigger('owl.prev');
    })

    sync7.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
    $(".next").click(function(){
        sync7.trigger('owl.next');
    })
    $(".prev").click(function(){
        sync7.trigger('owl.prev');
    })

    

    function syncPosition(el){
        var current = this.currentItem;
        $("#sync2")
        .find(".owl-item")
        .removeClass("synced")
        .eq(current)
        .addClass("synced")
        if($("#sync2").data("owlCarousel") !== undefined){
        center(current)
        }
    }

    $("#sync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
    });

    function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
        if(num === sync2visible[i]){
            var found = true;
        }
        }

        if(found===false){
        if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
        }else{
            if(num - 1 === -1){
            num = 0;
            }
            sync2.trigger("owl.goTo", num);
        }
        } else if(num === sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
        sync2.trigger("owl.goTo", num-1)
        }
        
    }

    // $('.close').click(function(){
    //     carousel.data('owlCarousel').jumpTo(1)
    // });

});


